from __future__ import division
import argparse
from collections import Counter, defaultdict
import editdist
import gzip


class Counts():

  def __init__(self, unrotated_fastq1, unrotated_fastq2, rotated_fastq1,
    rotated_fastq2, csv_in, prefix):
    "Initialise UDI counts."
    self.levenshtein = 1
    self.rpt         = {}
    self.i7s         = {}
    self.i5s         = {}
    self.load_csv_file(csv_in)
    self.counts1     = self.count_reads(unrotated_fastq1, unrotated_fastq2)
    self.counts2     = self.count_reads(rotated_fastq1, rotated_fastq2)
    self.lookup      = self.build_lookup()
    self.index_decode_rate(prefix)
    self.reporter_decode_rate(prefix, 'unrotated', self.counts1)
    self.reporter_decode_rate(prefix, 'rotated', self.counts2)
    self.test_hypotheses()


  def load_csv_file(self, csv_in):
    """Load data from csv file into dictionaries.
    Args:
      csv : csv filename
    """
    f = open(csv_in, 'r')
    # skip header
    f.readline()
    for line in f:
      line = line.strip().split(',')
      id  = int(line[1])
      rpt = line[2]
      i7  = line[3]
      i5  = line[4]
      self.rpt[rpt] = id
      self.i7s[i7]  = id
      self.i5s[i5]  = id


  def reverse_complement(self, sequence):
    "Return the reverse complement of sequence."
    alph = { 'A':'T', 'C':'G', 'G':'C', 'T':'A', 'N':'N' }
    return ''.join([alph[n] for n in sequence[::-1]])


  def get_match(self, query, known):
    """Return a match for the query sequence using the known dictionary 
    (within a Levenshtein edit distance of 2).
    """
    exact_match = known.get(query)
    if exact_match:
      return exact_match
    # inexact matches
    matches = []
    for k,v in known.items():
      if editdist.distance(query, k) <= self.levenshtein:
        matches.append(v)
    if len(matches) == 1:
      return matches.pop()
    return None


  def count_reads(self, fastq1, fastq2):
    """Count i5, i7, read1 reporter and read2 reporter matches from fastq
    files.
    Args:
      fastq1 : read1 fastq filename
      fastq2 : read2 fastq filename
    """
    counts = Counter()
    fq1 = gzip.open(fastq1, 'rb')
    fq2 = gzip.open(fastq2, 'rb')
    x = 0
    for header1 in fq1:
      header1   = header1.decode().strip()
      header2   = fq2.readline().decode().strip()
      sequence1 = fq1.readline().decode().strip()
      sequence2 = fq2.readline().decode().strip()
      # skip spacer and quality values
      for _ in range(2):
        fq1.readline()
        fq2.readline()
      # check read1 and read2 are in synchrony
      assert (header1.split(' ')[0] == header2.split(' ')[0])
      # get i5, i7, read1 reporter and read2 reporter sequences
      idx7_seq, idx5_seq = tuple(header1.split(' ')[1][6:].split('+'))
      rpt1_seq = sequence1[:12]
      rpt2_seq = self.reverse_complement(sequence2[:12])
      idx5_seq = self.reverse_complement(idx5_seq)
      # match i5, i7, rpt1, or rpt2 to a named sequence
      idx5_name = self.get_match(idx5_seq, self.i5s)
      idx7_name = self.get_match(idx7_seq, self.i7s)
      rpt1_name = self.get_match(rpt1_seq, self.rpt)
      rpt2_name = self.get_match(rpt2_seq, self.rpt)
      counts[(idx5_name, idx7_name, rpt1_name, rpt2_name)] += 1
    return counts


  def index_decode_rate(self, prefix):
    """Calculate the index decode rate as percentage of reads where i5 ≤ 
    edit distance AND i7 ≤ edit distance.
    """
    out = open(f'{prefix}.decode.rates.csv', 'w')
    out.write('platetype,decoded,total,percent.decoded\n')
    countdata = [('unrotated', self.counts1), ('rotated', self.counts2)]
    for platetype, counts in countdata:
      decoded = 0
      total   = 0
      for key in counts.keys():
        (idx5, idx7, rpt1, rpt2) = key
        if (idx5 and idx7):
          decoded += counts[key]
        total += counts[key]
      if total > 0:
        out.write(f'{platetype},{decoded},{total},{(decoded/total)*100}\n')
      else:
        out.write('Error: no reads')


  def build_lookup(self):
    "Build a quick lookup table to convert between experiments 1 and 2."
    n = [i for i in range(1,97)]
    return dict([(i,j) for i,j in zip(n, n[::-1])])


  def reporter_decode_rate(self, prefix, platetype, counts):
    """Calculate the number of reads associated with a given reporter that are
    associated with: (1) the expected UDI pair or (2) a different UDI pair.
    Assumes read1 and read2 reporters match, that idx5 and idx7 are
    decoded, and that idx5 and idx7 form a legitimate pair.
    """
    legitimate   = Counter()
    illegitimate = Counter()
    for key in counts.keys():
      (idx5, idx7, rpt1, rpt2) = key
      if (idx5 and
          idx7 and
          rpt1 and
          rpt2 and
          idx5 == idx7 and
          rpt1 == rpt2):
        if platetype == 'rotated':
          rpt1 = self.lookup.get(rpt1)
        if rpt1 == idx5:
          legitimate[rpt1] += counts[key]
        else:
          illegitimate[rpt1] += counts[key]
    # write out results
    out = open(f'{prefix}.{platetype}.contamination.csv', 'w')
    out.write('reporter,legitimate.UDI,illegitimate.UDI,total,'
              'illegitimate.percentage\n')
    for reporter in self.lookup.keys():
      legit   = legitimate[reporter]
      illegit = illegitimate[reporter]
      total   = legit + illegit
      if total > 0:
        percent = (1 - (legit/total)) * 100
        out.write(f'{reporter},{legit},{illegit},{total},{percent}\n')
    out.close()


  def simplify_counts(self, counts):
    """Simplify counts from (idx5, idx7, rpt1, rpt2) to (idx, rpt) provided
    idx5, idx7, rpt1 and rpt2 are not None and index5 and 7 match and reporter
    1 and 2 match.
    """
    simple = Counter()
    for key in counts.keys():
      (idx5, idx7, rpt1, rpt2) = key
      if (idx5 and
          idx7 and
          rpt1 and
          rpt2 and
          idx5 == idx7 and
          rpt1 == rpt2):
        simple[tuple([idx5, rpt1])] += counts[key]
    return simple


  def test_hypotheses(self):
    """Translate cases where the index and reporter sequences are different
    in experiment 1, test hypotheses 1 (index contamination) and 2 (reporter
    contamination) by finding equivalent coordinates in experiment2 and 
    reporting counts if > 0."""
    counts1 = self.simplify_counts(self.counts1)
    counts2 = self.simplify_counts(self.counts2)
    for (idx,rpt),count in counts1.items():
      print('unrotated',idx,rpt,count)
    for (idx,rpt),count in counts2.items():
      print('rotated',idx,rpt,count)
    print('hypothesis,noise1,noise2,signal1,signal2,percent')
    for idx in range(1,97):
      for rpt in range(1,97):
        if idx != rpt:
          # test hypothesis 1 (index contamination)
          noise_1 = counts1[(idx, rpt)]
          signal1 = counts1[(idx, idx)]
          noise_2 = counts2[(idx, self.lookup.get(rpt))]
          signal2 = counts2[(idx, self.lookup.get(idx))]
          if noise_1 > 0 and noise_2 > 0:
            percent = ((noise_1 + noise_2)/(noise_1 + noise_2 + signal1 + signal2)) * 100
            hythsis = f'Contamination of index {rpt} with index {idx}'
            print(f'{hythsis},{noise_1},{noise_2},{signal1},{signal2},{percent}')
          # test hypothesis 2 (reporter contamination)
          noise_1 = counts1[(idx, rpt)]
          signal1 = counts1[(rpt, rpt)]
          noise_2 = counts2[(self.lookup.get(idx), rpt)]
          signal2 = counts2[(self.lookup.get(rpt), rpt)]
          if noise_1 > 0 and noise_2 > 0:
            percent = ((noise_1 + noise_2)/(noise_1 + noise_2 + signal1 + signal2)) * 100
            hythsis = f'Contamination of reporter {idx} with reporter {rpt}'
            print(f'{hythsis},{noise_1},{noise_2},{signal1},{signal2},{percent}')


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Count indexes and reporters "
             "from index QC assay.")
  parser.add_argument("-n1", "--unrotated_fastq1", help="unrotated gzipped"
    "read1 fastq file", required=True)
  parser.add_argument("-n2", "--unrotated_fastq2", help="unrotated gzipped"
    "read2 fastq file", required=True)
  parser.add_argument("-r1", "--rotated_fastq1", help="rotated gzipped"
    "read1 fastq file", required=True)
  parser.add_argument("-r2", "--rotated_fastq2", help="rotated gzipped"
    "read2 fastq file", required=True)
  parser.add_argument("-c", "--csv", help="csv file of index and reporter "
    "sequences", required=True)
  parser.add_argument("-o", "--out", help="prefix of output filenames",
    required=True) 
  args = parser.parse_args()
  Counts(args.unrotated_fastq1, args.unrotated_fastq2, args.rotated_fastq1,
    args.rotated_fastq2, args.csv, args.out)
