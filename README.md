# README #

Index primer quality control.

### Process an index primer quality control dataset from iSeqs run including both a rotated and an unrotated plate ###
* python index_qc.py -n1 [unrotated gzipped read1 fastq file] -n2 [unrotated gzipped read2 fastq file] -r1 [rotated gzipped read1 fastq file] -r2 [rotated gzipped read2 fastq file] -c [csv file containing index and reporter sequences] -o [prefix for output files]
* The Levenshtein edit distance is set to 1, equivalent to bcl2fastq conversion in production

### Input ###
* bcl2fastq an iSeq run into two fastq.gz files (one for read 1 and one for read 2).
* fastq must include a header line with index 1 and 2 sequences e.g. @FS10000580:33:BPL20322-0920:1:1101:1030:1000 1:N:0:CATAAGGTGC+ATAGCGAGCT
* csv file containing index and reporter sequences has headers: well, id, reporter, i7, i5 (reverse complement header line i5 read)

### Output ###
* {prefix}.decode.rates.csv
Calculate the total number of paired-end reads where indexes match a known i5 AND known i7 sequence. Note that the i5/i7 index sequences do not necessarily need to be in the  correct combination. File header: platetype,decoded,total,percent_decoded

* {prefix}.unrotated.contamination.csv
For the unrotated plate count the number of reads for each reporter associated with the correct i5/i7 index combination ('legitimate') versus the incorrect i5/i7 combination ('illegitimate'). File header: reporter,legitimate.UDI,illegitimate.UDI,total,illegitimate.percentage

* {prefix}.rotated.contamination.csv
For the rotated plate count the number of reads for each reporter associated with the correct i5/i7 index combination ('legitimate') versus the incorrect i5/i7 combination ('illegitimate'). File header: reporter,legitimate.UDI,illegitimate.UDI,total,illegitimate.percentage

* stdout prints hypotheses showing index and/or reporter contamination events